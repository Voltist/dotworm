import pickle as pkl
import numpy as np
from PIL import Image
from scipy.misc import imresize
from PIL import ImageFont
from PIL import ImageDraw
import pandas as pd
import sys

import connectome

df = pd.read_csv("connectome.csv")

data = {}
for index, row in df.iterrows():
    if row["Type"] == "Send":

        if row["Origin"] not in data.keys():
            data[row["Origin"]] = [[row["Target"], row["Number of Connections"]]]

        else:
            data[row["Origin"]].append([row["Target"], row["Number of Connections"]])

ndata = data

with open("data/" + sys.argv[1] + ".pkl", "rb") as fp:
    data = pkl.load(fp)

nlist = []
for i in sorted(data["neurons"].keys()):
    muscle = False
    for x in connectome.muscles:
        if x in i:
            muscle = True

    if not muscle:
        nlist.append(i)

targets = sys.argv[2].split(",")

inputs = []
for i in nlist:
    for x in targets:
        if i in ndata.keys():
            if x in zip(*ndata[i])[0]:
                inputs.append(i)
                break

outputs = []
for i in targets:
    outputs += zip(*ndata[i])[0]

out = []
for i in sorted(nlist):

    if i in targets or i in inputs or i in outputs:

        tmp = []
        for x in data["neurons"][i]:

            val = 255.0
            if x >= 30:

                if i in targets:
                    tmp.append((0, val, 0))
                elif i in inputs:
                    tmp.append((val, 0, 0))
                elif i in outputs:
                    tmp.append((0, 0, val))
                else:
                    tmp.append((val, val, val))

            else:
                tmp.append((0, 0, 0))

        out.append(tmp)

# Padding
for i in range(10):
    out.append([(0, 0, 0) for x in range(len(data["neurons"]["ASEL"]))])

rolling_average = []
for index, i in enumerate(data["distance"]):

    tmp = []
    for x in range(-50, 50):
        try:
            tmp.append(data["distance"][index+x])
        except IndexError:
            continue

    rolling_average.append(int((float(sum(tmp)) / len(tmp)) * 500))

last = 0
periods = []
tmp = [[0, 0, 0]]
for index, x in enumerate(rolling_average):

    if x > last:
        type = 1
    elif x < last:
        type = -1
    else:
        type = 0

    if type == tmp[-1][0]:
        tmp[-1][2] += 1
    else:
        tmp.append([type, index, index])

    last = x

new_tmp = [(0, 0, 0) for i in range(len(rolling_average))]
for i in tmp:
    if i[2] - i[1] >= 5:
        if i[0] == 1:
            for x in range(i[1], i[2]):
                new_tmp[x] = (0, 255, 0)
        elif i[0] == -1:
            for x in range(i[1], i[2]):
                new_tmp[x] = (255, 0, 0)

for i in range(3):
    out.append(new_tmp)

tmp = []
for x in data["distance"]:
    if x != 666:
        tmp.append((255*x, 20*x, 147*x))
    else:
        tmp.append((255, 255, 255))

for i in range(3):
    out.append(tmp)

# Left padding
for i in range(len(out)):
    out[i] = [(0, 0, 0) for x in range(3)] + out[i]

newimg = imresize(np.array(out), interp="nearest", size=1000)
img = Image.fromarray(np.uint8(newimg) , 'RGB')

draw = ImageDraw.Draw(img)
fnt = ImageFont.truetype('fonts/ubuntu/UbuntuMono-R.ttf', 10)

mod = 0
for index, i in enumerate(sorted(nlist)):
    if i in targets or i in inputs or i in outputs:
        draw.text((3, (index*10)-mod), str(i), font=fnt, fill=(255, 255, 255))
    else:
        mod += 10

img.save("out.png")
