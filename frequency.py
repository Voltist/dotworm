import pickle as pkl
import matplotlib.pyplot as plt
import sys
import seaborn as sns
import math
import matplotlib.patches as mpatches
import connectome

sns.set(style="dark")

def distance(p1, p2):
    return math.sqrt((p1[0] - p2[0])**2 + (p1[1] - p2[1])**2)

def proc(command):
    global buffer

    cSplit = command.split(" ")

    if cSplit[0] == "load":
        with open("data/" + cSplit[1] + ".pkl", "rb") as fp:
            tmp = pkl.load(fp)["neurons"]

        nlist = []
        for i in sorted(tmp.keys()):
            muscle = False
            for x in connectome.muscles:
                if x in i:
                    muscle = True

            if not muscle and i not in nlist:
                nlist.append(i)

        buffer[cSplit[1]] = {}
        for i in tmp.keys():
            if i in nlist:
                buffer[cSplit[1]][i] = 0

                for x in tmp[i]:
                    if x >= 30:
                        buffer[cSplit[1]][i] += 1

        print "Loaded to buffer..."

    elif cSplit[0] == "list":
        print "\n".join(buffer.keys())

    elif cSplit[0] == "zero":
        for i in cSplit[2:]:
            buffer[cSplit[1]][i] = 0

    elif cSplit[0] == "graph":
        plt.clf()

        graphs = []
        annotations = []
        for i in cSplit[1:]:
            for index, x in enumerate(sorted(buffer[i].keys())):
                if buffer[i][x] > 60 or buffer[i][x] < -350:
                    try:
                        annotations.append([index, buffer[i][x], x])
                    except IndexError:
                        continue

        done = []
        for index, i in enumerate(annotations):
            if i[1] > 0:
                mod = 2
            else:
                mod = -2

            if i[2][:-1] not in done:
                plt.text(i[0], ((i[1]/2000.0)*100)+mod, i[2], ha="center", va="center")

                done.append(i[2][:-1])

        for i in cSplit[1:]:
            tmp = []
            for x in sorted(buffer[i].keys()):
                tmp.append( (buffer[i][x]/2000.0) * 100 )
            graphs.append(plt.plot(tmp, label=i.replace("_", " "))[0])

        plt.legend(handles=graphs)
        plt.xlabel('Neuron ID')
        plt.ylabel('Firings (% of Epochs)')
        plt.savefig("outputs/" + sys.argv[1] + ".png")
        plt.show()

    elif cSplit[0] == "sub":
        out = {}

        for i in buffer[cSplit[1]].keys():
            out[i] = buffer[cSplit[1]][i] - buffer[cSplit[2]][i]

        buffer[cSplit[3]] = out
        print "Saved to buffer..."

buffer = {}

if len(sys.argv) == 2:
    with open("data/" + sys.argv[1] + ".txt", "r") as fp:
        for i in fp.read().strip("\n").split("\n"):
            if i != "":
                proc(i)

else:
    while True:
        command = raw_input("> ")
        proc(command)
