# Liam Scaife's Dotworm
# Adapted (but not forked) from GoPiGo Connectome by Open Connectome Engine

import time
import copy
import pygame
from pygame.locals import *
import pygame.gfxdraw
import math
import sys
import pygame.freetype
import pickle as pkl
import random

# Init pygame window
pygame.init()
size = width, height = 1366,738
screen = pygame.display.set_mode(size)
GAME_FONT = pygame.freetype.Font("fonts/ubuntu/UbuntuMono-R.ttf", 100)

# Location and current rotation
loc = [1920/2, 1080/2]
history = []
rot_history = []
rot = 0

# The postSynaptic dictionary contains the accumulated weighted values as the
# connectome is executed
postSynaptic = {}

# For storage of neuron data to disk
data = {"neurons": {}, "distance": []}

# Import connectome
from connectome import *

global thisState
global nextState
global postSynaptic
thisState = 0
nextState = 1

# The Threshold is the maximum sccumulated value that must be exceeded before
# the Neurite will fire
threshold = 30

# Accumulators are used to decide the value to send to the Left and Right motors
# of the GoPiGo robot
accumleft = 0
accumright = 0

# Function that finds the distance between two points
def distance(p1, p2):
    return math.sqrt((p1[0] - p2[0])**2 + (p1[1] - p2[1])**2)

# Function that splits list into evenly sized chunks
def chunks(l, n):
    for i in range(0, len(l), n):
        yield l[i:i + n]

def motorcontrol():
    global rot
    global accumright
    global accumleft

    # accumulate left and right muscles and the accumulated values are
    # used to move the left and right motors of the robot
    for muscle in muscleList:
    	if muscle in mLeft:
    	   accumleft += postSynaptic[muscle][nextState]
    	   postSynaptic[muscle][nextState] = 0
    	elif muscle in mRight:
    	   accumright += postSynaptic[muscle][nextState]
    	   postSynaptic[muscle][nextState] = 0

    # We turn the wheels according to the motor weight accumulation
    new_speed = abs(accumleft) + abs(accumright)

    if new_speed > 100:
    	new_speed = 100

    speed = new_speed / 10

    rot_delta = 90 - int((accumright + 0.001) / (accumleft + 0.001) * 90)
    rot += rot_delta

    rot_history.append(rot)
    if len(rot_history) > 3:
        del rot_history[0]

    loc[0] += math.sin(math.radians(rot)) * speed
    loc[1] += math.cos(math.radians(rot)) * speed

    history.append(copy.copy(loc))

    l = 0
    for i in list(chunks(history, 2)) + list(chunks(history[1:], 2)):
        if len(i) == 2:
            pygame.gfxdraw.aaellipse(screen, int(i[0][0]), int(i[0][1]), 10, 10, pygame.Color(255, 255, 255, 255))
            pygame.gfxdraw.filled_ellipse(screen, int(i[0][0]), int(i[0][1]), 10, 10, pygame.Color(255, 255, 255, 255))

            l += distance(i[0], i[1])

            if l > 200:
                del history[0]

    for index, i in reversed(list(enumerate(history))[1:-1]):

        av_x = (history[index-1][0] + history[index-1][0]) / 2
        av_y = (history[index-1][1] + history[index-1][1]) / 2

        new_x = (i[0] + av_x) / 2
        new_y = (i[1] + av_y) / 2

        history[index] = [new_x, new_y]

    accumleft = 0
    accumright = 0

def dendriteAccumulate(dneuron):
    global postSynaptic
    f = eval(dneuron)
    postSynaptic = f(postSynaptic, nextState)

def fireNeuron(fneuron):
    global postSynaptic
    # The threshold has been exceeded and we fire the neurite
    if fneuron != "MVULVA":
    	f = eval(fneuron)
    	postSynaptic = f(postSynaptic, nextState)
    	postSynaptic[fneuron][nextState] = 0

def runconnectome():
    # Each time a set of neuron is stimulated, this method will execute
    # The weigted values are accumulated in the postSynaptic array
    # Once the accumulation is read, we see what neurons are greater
    # then the threshold and fire the neuron or muscle that has exceeded
    # the threshold
    global thisState
    global nextState

    if len(sys.argv) == 4:
        inh = sys.argv[3].split(",")
    else:
        inh = []

    for ps in postSynaptic.keys():
        if ps[:3] not in muscles and ps not in inh and abs(postSynaptic[ps][thisState]) >= threshold:
        	fireNeuron(ps)

    for ps in postSynaptic.keys():
    	if ps[:3] not in muscles:

            # Append neuron state to data for storage to disk
            data["neurons"][ps].append(postSynaptic[ps][thisState])

    motorcontrol()
    pygame.display.update()

    for ps in postSynaptic:
    	postSynaptic[ps][thisState] = copy.deepcopy(postSynaptic[ps][nextState]) #fired neurons keep getting reset to previous weight

    thisState,nextState = nextState,thisState

postSynaptic = createpostSynaptic(postSynaptic)
for i in postSynaptic.keys():
    data["neurons"][i] = []

epoch = 0
while epoch < int(sys.argv[1]):
    screen.fill((0, 0, 0))

    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
        if event.type == KEYDOWN and event.key == K_q:

            pygame.quit()
            sys.exit()

    # Stimulate hunger neurons
    postSynaptic["AIBL"][nextState] += 30
    postSynaptic["AIBR"][nextState] += 30

    point = [0, height]
    if distance(loc, point) <= (width*1.23):
        val = (1 - (float(distance(loc, point)) / (width*1.25)))

        data["distance"].append(val)

        pygame.gfxdraw.aaellipse(screen, point[0], point[1], 10, 10, pygame.Color(255, 0, 0, 255))
        pygame.gfxdraw.filled_ellipse(screen, point[0], point[1], 10, 10, pygame.Color(255, 0, 0, 255))

        GAME_FONT.render_to(screen, (10, 10), str(int(val*100)), (255, 255, 255))

        postSynaptic["ASEL"][nextState] += val * 0
        postSynaptic["ASER"][nextState] += val * 0

    else:
        data["distance"].append(0)

    off = True
    if loc[0] < 0:
        loc[0] = 0
    elif loc[0] > width:
        loc[0] = width
    elif loc[1] < 0:
        loc[1] = 0
    elif loc[1] > height:
        loc[1] = height
    else:
        off = False

    if off:
        for i in "FLPR, FLPL, ASHL, ASHR, IL1VL, IL1VR, OLQDL, OLQDR, OLQVR, OLQVL".split(", "):
            postSynaptic[i][nextState] += 30

    runconnectome()

    epoch += 1

print max(data["distance"]) * 100.0
with open("data/" + sys.argv[2] + ".pkl", "w") as fp:
    pkl.dump(data, fp)
