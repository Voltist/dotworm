import pickle as pkl
import matplotlib.pyplot as plt
import sys
import seaborn as sns
import math
import matplotlib.patches as mpatches

sns.set(style="dark", palette="muted")

def distance(p1, p2):
    return math.sqrt((p1[0] - p2[0])**2 + (p1[1] - p2[1])**2)

def proc(command):
    global buffer
    cSplit = command.split(" ")

    if cSplit[0] == "load":
        with open("freq/data/" + cSplit[1] + ".pkl", "rb") as fp:
            buffer[cSplit[1]] = pkl.load(fp)
        print "Loaded to buffer..."

    elif cSplit[0] == "list":
        print "\n".join(buffer.keys())

    elif cSplit[0] == "zero":
        for i in cSplit[2:]:
            buffer[cSplit[1]][i] = 0

    elif cSplit[0] == "graph":
        plt.clf()

        graphs = []
        annotations = []
        for i in cSplit[1:]:
            for index, x in enumerate(sorted(buffer[i].keys())):
                if buffer[i][x] > 1000 or buffer[i][x] < -1000:
                    if abs(buffer[i][x]) > abs(buffer[i][sorted(buffer[i].keys())[index-1]]) and abs(buffer[i][x]) > abs(buffer[i][sorted(buffer[i].keys())[index+1]]):
                        annotations.append([index, buffer[i][x], x])

        done = []
        for index, i in enumerate(annotations):
            if i[1] > 0:
                mod = 500
            else:
                mod = -500

            if i[2] not in done:
                plt.text(i[0], i[1]+mod, i[2], ha="center", va="center")

                vals = []
                for lol in cSplit[1:]:
                    vals.append(buffer[lol][i[2]])

                plt.plot([i[0], i[0]], [max(vals), min(vals)], color="lightgray")
                done.append(i[2])

        for i in cSplit[1:]:
            tmp = []
            for x in sorted(buffer[i].keys()):
                tmp.append(buffer[i][x])
            graphs.append(plt.plot(tmp, label=i.replace("_", " "))[0])

        plt.legend(handles=graphs)
        plt.axis('off')
        plt.show()

    elif cSplit[0] == "sub":
        out = {}

        for i in buffer[cSplit[1]].keys():
            out[i] = buffer[cSplit[1]][i] - buffer[cSplit[2]][i]

        buffer[cSplit[3]] = out
        print "Saved to buffer..."

buffer = {}

if len(sys.argv) == 2:
    with open("freq/" + sys.argv[1] + ".txt", "r") as fp:
        for i in fp.read().strip("\n").split("\n"):
            if i != "":
                proc(i)

else:
    while True:
        command = raw_input("> ")
        proc(command)
