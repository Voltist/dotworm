import pickle as pkl
import sys
import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit
import math
import seaborn as sns
import copy

sns.set(style="dark")

for n in sys.argv[1].split(","):

    X = []
    Y = []

    for name in range(30):
        print int((name/30.0)*100)

        with open("data/3000_" + str(name) + ".pkl", "rb") as fp:
            data = pkl.load(fp)
        for index, i in enumerate(data["neurons"][n]):
            if i >= 30:

                val_left = 1
                for x in range(1, 3000):
                    try:
                        if data["neurons"][n][index-x] >= 30:
                            val_left = x
                            break
                    except IndexError:
                        break

                val_right = 1
                for x in range(1, 3000):
                    try:
                        if data["neurons"][n][index+x] >= 30:
                            val_right = x
                            break
                    except IndexError:
                        break

                freq = (val_left + val_right) / 2.0

                if data["distance"][index] != 0 and ((freq / 2000.0) * 100) <= 4.0:

                    X.append(data["distance"][index] * 100)
                    Y.append((freq / 2000.0) * 100)

    sns.scatterplot(X, Y, alpha=0.5, label=n)

plt.ylabel(sys.argv[1].replace(",", " and ") + " Firing Sparsity (% of Total Epochs)")

plt.xlabel("Stimulation of ASE (%)")

plt.savefig("outputs/" + sys.argv[1].replace(",", "_") + "_static.png")
