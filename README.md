# dotWorm

A simple but effective simulation of the __Caenorhabditis elegans__ worm brain that successfully simulates chemotaxis and social feeding behaviours.

## Getting Started

Connectome data is included, so just run install the prerequisites and run the primary script.

### Prerequisite Packages

* Pygame
* Numpy
* Math

### Usage

Run the main script like so:
`python2 main.py <num epochs> <name of simulation>`
Pick a sensible name for the simulation so that you can process the data later.

## Neural Function and Behavior

To be described. In the mean time, look at the code yourself. It mostly is based off of the GoPiGo connectome.

### Locomotion Approximation

Real worms wiggle in order to move through soil or liquids. Fluid dynamics
is complex and computationally expensive, so here an alternative is used; muscle contraction levels on the
left and right side of the non-existent worm body (excluding head muscles) are summed
together and used to steer the worms you see on screen. This approximation of the worm
movement seems to be accurate.

## Contributing

Contributions are welcome, just make a pull request!

## Authors

* **Liam Scaife** - *Everything so far* - [Gitlab](https://gitlab.com/Voltist) [Github](https://github.com/dizzyliam)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
