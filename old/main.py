import pandas as pd
import pygame
import math
import time
from pygame.locals import *
import numpy as np
import random
import sys
import argparse
import pickle as pkl

parser = argparse.ArgumentParser(description='Worm brain simulation.')
parser.add_argument('--social', dest='social', type=int, default=0,
                    help='sociality modifier')
parser.add_argument('--worms', dest='worms', type=int, default=1,
                    help='number of worms')
parser.add_argument('--pos', dest='pos', type=int, default=0,
                    help='starting position offset')
parser.add_argument('--food', dest='food', type=int, default=1,
                    help='is there food? 1: yes, 0: no')
parser.add_argument('--name', dest='name', type=str, default="",
                    help='name of data to be saved')
parser.add_argument('--epochs', dest='epochs', type=int, default=0,
                    help='number of epochs to run for (0 for forever)')
parser.add_argument('--type', dest='type', type=str, default="none",
                    help='type of data to gather (freq/pattern/none)')
parser.add_argument('--inh', dest='inh', type=str, default="",
                    help='inhibit these comma-seperated neurons')

args = parser.parse_args()

def rotate(origin, point, angle):
    """
    Rotate a point counterclockwise by a given angle around a given origin.

    The angle should be given in radians.
    """
    ox, oy = origin
    px, py = point

    qx = ox + math.cos(angle) * (px - ox) - math.sin(angle) * (py - oy)
    qy = oy + math.sin(angle) * (px - ox) + math.cos(angle) * (py - oy)
    return qx, qy

def check(point, size):
    if point[0] < 0:
        return 0
    elif point[1] < 0:
        return 1
    elif point[0] > width:
        return 2
    elif point[1] > height:
        return 3
    else:
        return -1

    return (dir, loc, left_loc, right_loc, front)

def gb(x, y, center_x, center_y):
        angle = math.degrees(math.atan2(y - center_y, x - center_x))
        return angle

def distance(p1, p2):
    return math.sqrt((p1[0] - p2[0])**2 + (p1[1] - p2[1])**2)

def rotate_list(l, n):
    return l[n:] + l[:n]

df = pd.read_csv("data/connectome.csv")
ms = pd.read_csv("data/muscle.csv")

nlist = list(set(df["Origin"].tolist() + df["Target"].tolist()))
mlist = list(set(ms["Muscle"].tolist()))

freq_data = {}
for i in nlist:
    freq_data[i] = 0

pattern_data = [{}, {}, {}, {}][0:args.worms]
for i in nlist:
    for x in range(args.worms):
        pattern_data[x][i] = []

muscle_data = [{}, {}, {}, {}][0:args.worms]
for i in mlist:
    for x in range(args.worms):
        muscle_data[x][i] = []

scores = [{}, {}, {}, {}][0:args.worms]
for x in range(args.worms):
    scores[x]["dist"] = []
    scores[x]["x"] = []
    scores[x]["y"] = []

data = {}
mlinks = {}

for index, row in df.iterrows():
    if row["Type"] == "Send":

        if row["Origin"] not in data.keys():
            data[row["Origin"]] = [[row["Target"], row["Number of Connections"]]]

        else:
            data[row["Origin"]].append([row["Target"], row["Number of Connections"]])

dlist = []
for index, row in df.iterrows():
    if row["Neurotransmitter"] == "Dopamine":
        dlist.append(row["Target"])

for index, row in ms.iterrows():
    if row["Neuron"] not in mlinks.keys():
        mlinks[row["Neuron"]] = [[row["Muscle"], row["Number of Connections"]]]
    else:
        mlinks[row["Neuron"]].append([row["Muscle"], row["Number of Connections"]])

pygame.init()
size = width, height = 1000, 500
screen = pygame.display.set_mode(size)

worms = []
for i in rotate_list([[0, 0]], args.pos)[0:args.worms]:
    worm = {}
    worm["loc"] = i
    worm["rot"] = 0
    worm["speed"] = 0

    worm["state"] = {}
    for i in nlist:
        worm["state"][i] = 0

    worm["mstate"] = {}
    for i in mlist:
        worm["mstate"][i] = 0

    worms.append(worm)

foods = []
if args.food == 1:
    for i in range(100):
        rr = random.choice(range(360))
        foods.append(rotate([width/2, height/2], [(width/2) + i, (height/2)], rr))

food_got = 0
soc = args.social
lastv = 12
last = None
epoch = 0
while True:
    print epoch, food_got

    if len(foods) == 0:
        if args.food == 1:
            for i in range(100):
                rr = random.choice(range(360))
                foods.append(rotate([width/2, height/2], [(width/2) + i, (height/2)], rr))

    if args.epochs != 0 and epoch >= args.epochs:
        if args.name != "":
            if args.type == "freq":
                with open("freq/data/" + args.name + ".pkl", "wb") as fp:
                    pkl.dump(freq_data, fp)
            elif args.type == "pattern":
                with open("pattern/data/" + args.name + ".pkl", "wb") as fp:
                    pkl.dump(pattern_data, fp)
                with open("pattern/data/" + args.name + "_fdist.pkl", "wb") as fp:
                    pkl.dump(scores, fp)
                with open("pattern/data/" + args.name + "_muscles.pkl", "wb") as fp:
                    pkl.dump(muscle_data, fp)

        break

    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
        if event.type == KEYDOWN and event.key == K_q:

            pygame.quit()
            sys.exit()

    for windex, worm in enumerate(worms):

        scores[windex]["x"].append(worm["loc"][0])
        scores[windex]["y"].append(worm["loc"][1])

        if epoch < 20:
            worm["state"]["ASEL"] += 15
            worm["state"]["ASER"] += 15

            for n in "ASHL, ASHR, ADLL, ADLR".split(", "):
                worm["state"][n] += 15

        for n in "RIML, RIMR, RICL, RICR".split(", "):
            worm["state"][n] += 10

        # Food
        fdel = []
        vals = [0]
        got = False
        for index, food in enumerate(foods):
            val = int((1 - (distance(worm["loc"], food) / height)) * 15)

            if distance(worm["loc"], food) <= 20:
                fdel.append(index)
                food_got += 1
                got = True
                #for i in dlist:
                #    state[i] += [epoch for x in range(15)]

            if distance(worm["loc"], food) <= height:
                vals.append(val)

        for n in "ASEL, ASER".split(", "):
            worm["state"][n] += max(vals)

        if not got:
            scores[windex]["dist"].append(max(vals))
        else:
            scores[windex]["dist"].append(666)

        # Other worms
        vals = [0]
        for index, w2 in enumerate(worms):
            if w2 != worm:
                val = int((1 - (distance(worm["loc"], w2["loc"]) / height)) * soc)

                if distance(worm["loc"], w2["loc"]) <= height:
                    vals.append(val)

        for n in "ASHL, ASHR, ADLL, ADLR".split(", "):
            worm["state"][n] += max(vals)

        nfoods = []
        for index, i in enumerate(foods):
            if index not in fdel:
                nfoods.append(i)
        foods = nfoods

        newstate = worm["state"]
        screen.fill(pygame.Color("black"))

        for i in nlist:

            if i in args.inh.split(","):
                worm["state"][i] = 0

            if args.type == "pattern":
                pattern_data[windex][i].append(worm["state"][i])

            if worm["state"][i] >= 15:

                if args.type == "freq":
                    freq_data[i] += 1.0

                if i in mlinks.keys():
                    for x in mlinks[i]:
                        worm["mstate"][x[0]] += x[1]

                if i not in data.keys():
                    continue

                for x in data[i]:
                    newstate[x[0]] += x[1]

                newstate[i] = 0

            else:
                newstate[i] = float(worm["state"][i]) * 0.7

        for i in sorted(mlist):
            worm["mstate"][i] = float(worm["mstate"][i]) * 0.7
            muscle_data[windex][i].append(worm["mstate"][i])

        leftsum = 0
        rightsum = 0
        for i in mlist:
            if ("MD" in i or "MV" in i) and "VU" not in i and int(i[-2:]) > 6:
                if "MDL" in i or "MVL" in i:
                    leftsum += worm["mstate"][i]
                if "MDR" in i or "MVR" in i:
                    rightsum += worm["mstate"][i]

        target_dir = worm["rot"] + ((leftsum - rightsum) / 19 * math.pi)

        speed_target = (abs(leftsum) + abs(rightsum)) / 100.0
        speed_interval = speed_target / 30.0
        worm["speed"] += speed_interval;

        minus = worm["rot"] - target_dir
        diff = minus

        if abs(minus) > 180:
            if worm["rot"] > target_dir:
                diff = -1 * ((360 - worm["rot"]) + target_dir)
            else:
                diff = (360 - target_dir) + worm["rot"]

        if diff > 0:
            worm["rot"] -= 0.1
        elif diff < 0:
            worm["rot"] += 0.1

        xc = math.cos(worm["rot"]) * worm["speed"]
        yc = math.sin(worm["rot"]) * worm["speed"]

        if not (worm["loc"][0]+xc < 0 or worm["loc"][0]+xc > width):
            worm["loc"][0] += xc
        else:
            last = "ASHL, ASHR, FLPL, FLPR, OLQDL, OLQDR, OLQVL, OLQVR"
            for i in last.split(", "):
                newstate[i] += 15

        if not (worm["loc"][1]+yc < 0 or worm["loc"][1]+yc > height):
            worm["loc"][1] += yc
        else:
            last = "ASHL, ASHR, FLPL, FLPR, OLQDL, OLQDR, OLQVL, OLQVR"
            for i in last.split(", "):
                newstate[i] += 15

        worm["state"] = newstate

    for worm in worms:
        fp = [worm["loc"][0]-15, worm["loc"][1]]
        bp = [worm["loc"][0]+15, worm["loc"][1]]

        fp = rotate(worm["loc"], fp, math.radians(worm["rot"]))
        bp = rotate(worm["loc"], bp, math.radians(worm["rot"]))

        pygame.draw.line(screen, pygame.Color(0, 255, 0, 255), tuple(fp), tuple(bp), 10)


    for f in foods:
        pygame.draw.circle(screen, pygame.Color(255, 0, 0, 255), (int(f[0]), int(f[1])), 5)

    pygame.display.update()
    epoch += 1
