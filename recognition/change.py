import pickle as pkl
import sys
import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit
import math
import seaborn as sns
import copy
from connectome import muscleList

sns.set(style="dark")

data = {}
print "Loading..."
for name in range(30):
    print int((name/30.0)*100)

    with open("data/3000_" + str(name) + ".pkl", "rb") as fp:
        data[name] = pkl.load(fp)

X = []
for x in "AIB, RIM, AVB, AVA, DA, AS, VA".split(", "):
    for i in data[0]["neurons"].keys():
        try:
            if i.index(x) == 0 and i not in muscleList and "ASE" not in i and "ASI" not in i:
                X.append(i)
                break
        except ValueError:
            continue

Y0 = []
Y1 = []

for n in X:

    y0 = 0
    y1 = 0

    tmp = []
    for name in range(30):
        print int((name/30.0)*100)

        for index, i in enumerate(data[name]["neurons"][n]):
            if i >= 30:

                if index > 10:
                    if data[name]["distance"][index] > data[name]["distance"][index-10]:
                        y0 += 1
                    elif data[name]["distance"][index] < data[name]["distance"][index-10]:
                        y1 += 1

    Y0.append((y0/60000.0) * 100)
    Y1.append((y1/60000.0) * 100)

fordel = []
for index, i in enumerate(X):
    done = False

    for xindex, x in enumerate(X[0:index]):
        try:
            if i.index(x) == 0:
                done = True
                break
        except ValueError:
            continue

    if done:
        print "Averaging:", index, xindex
        fordel.append(i)

        Y0[xindex] = (Y0[xindex] + Y0[index]) / 2.0
        Y1[xindex] = (Y1[xindex] + Y1[index]) / 2.0

    else:
        X[index] = i[:-1]

for i in fordel:
    index = X.index(i)
    del X[index]
    del Y0[index]
    del Y1[index]

x = np.arange(len(X))
width = 0.35

fig, ax = plt.subplots()
ax.bar(x - width/2, Y0, width, label="Ascent")
ax.bar(x + width/2, Y1, width, label="Descent")

ax.set_xlabel("Neuron Group")
ax.set_ylabel("Firings (% of Epochs)")

ax.set_xticks(x)
texts = ax.set_xticklabels(X)
ax.legend()

plt.savefig("outputs/change.png")
plt.show()
